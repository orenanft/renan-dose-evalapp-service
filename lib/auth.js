'use strict';

const Promise = require('bluebird');
const moment = require('moment');
const Utils = require('fwsp-jsutils');
const jwtAuth = require('fwsp-jwt-auth');
const mysqldb = require('../lib/mysql');

jwtAuth.loadCerts('./server.pem', './server.pub');
/**
* @name Auth
* @summary auth class library
*/
class Auth {
  /**
  * @name constructor
  * @summary Auth constructor
  * @return {undefined}
  */
  constructor() {
  }

  /**
  * @name login
  * @summary login validating userName and password
  * @param {string} email - user email
  * @param {string} password - password
  * @return {object} promise - resolves to result or error
  */
  login(email, password) {
    return new Promise((resolve, reject) => {
      let userDoc;
      let hashedPassword = Utils.md5Hash(password);

      mysqldb.query( 'SELECT * FROM usuario where email=\''+email+'\' and senha=MD5(\''+password+'\')' )
      .then( row => {
        if (!row[0]) {
            resolve({
              isValid: false
            });
            return;
          }
          let user = row[0];
          let userDoc = {
            isValid: true,
            id: user.id,
            email: user.email,
          };

        jwtAuth.createToken(userDoc).then((token) => {
          // token is now ready for use.
          let ts = moment().unix();
          mysqldb.query( 'UPDATE usuario SET last_login=\''+ts+'\', token=\''+token+'\'where email=\''+userDoc.email+'\'' )
          .then( row => {
            resolve({
                  isValid: (userDoc !== null),
                  user: userDoc,
                  lastLogin: ts,
                  shortToken: jwtAuth.getTokenHash(token),
                  token: token
              });
            return;
          } )
         .catch( err => { resolve({isValid: false}); })
       })
        } )
      .catch( err => { resolve({isValid: false}); })
    });
  }

  /**
  * @name logout
  * @summary logout user with valid token
  * @param {string} token - JWT token
  * @return {object} promise - resolves to result or error
  */
  logout(token) {
    let sql = 'SELECT * FROM usuario where token=\''+token+'\'';

    return new Promise((resolve, reject) => {
      mysqldb.query( 'SELECT * FROM usuario where token=\''+token+'\'' )
      .then( row => {
        if (!row[0]) {
          resolve({isValid: false});
          return;
        }
        mysqldb.query( 'UPDATE usuario SET token=NULL where token=\''+token+'\'' )
        .then( row => {
          resolve({isValid: true});
          return;
        } )
       .catch( err => { resolve({isValid: false}); })
      } )
     .catch( err => { resolve({isValid: false}); })
    });
  }

}

module.exports = Auth;
