'use strict';

const Promise = require('bluebird');
const request = require('request');
const mysqldb = require('../lib/mysql');

/**
* @name buscaCep
* @summary Helper function to access viacep ws
* @param {string} cep - user cep
* @return {object} promise
*/
function buscaCep(cep){
  return new Promise((resolve, reject) => {
    request.get("http://viacep.com.br/ws/"+cep+"/json/unicode/", (error, response, body) => {
    resolve (body);
    });
  });
  }

/**
* @name Crud
* @summary Crud class library
*/
class Crud {
  /**
  * @name constructor
  * @summary Crud constructor
  * @return {undefined}
  */
  constructor() {
  }

  /**
  * @name listById
  * @summary list user by id
  * @param {string} id - user id
  * @return {object} promise - resolves to result or error
  */



  listById(id) {
    return new Promise((resolve, reject) => {

      mysqldb.query( 'SELECT * FROM usuario WHERE id='+id )
      .then( row => {

        if (!row[0]) {
            resolve({
              isValid: false
            });
            return;
          }

          let user = row[0];

          buscaCep(user.cep).then( infoCep =>{

            resolve({
                  isValid: true,
                  email: user.email,
                  cep: user.cep,
                  buscaCep: infoCep
              });

            return;
            }
          ).catch(error => reject(error))

        })
      .catch(err => { resolve({isValid: false}); })
    });
  }

  /**
  * @name list
  * @summary list users
  * @return {object} promise - resolves to result or error
  */
  list() {
    return new Promise((resolve, reject) => {

      mysqldb.query( 'SELECT * FROM usuario' )
      .then( row => {

        if (!row) {
            resolve({
              isValid: false
            });
            return;
          }
          let users = row;
          let promises = [];
          let usersList = [];
          users.forEach((item) =>{
            promises.push(buscaCep(item.cep).then( infoCep =>{
              let user = {
                email: item.email,
                cep: item.cep,
                buscaCep: infoCep
              };
              usersList.push(user);
            }).catch(error => reject(error)))
            })

            Promise.all(promises).then((result) => {
            resolve({
                  isValid: true,
                  users: usersList
              });

            return;

          })
        } )
      .catch(err => { resolve({isValid: false}); })
    });
  }

  /**
  * @name updateCepById
  * @summary update user cep by id
  * @param {string} id - user id
  * @param {string} cep - user cep
  * @return {object} promise - resolves to result or error
  */
  updateCepById(id, cep) {
    return new Promise((resolve, reject) => {

      mysqldb.query( 'UPDATE usuario SET cep='+cep+' WHERE id='+id )
      .then( row => {
          resolve({
              success: true
            });
            return;
        }, err => {
        resolve({success: false});
        })
      .catch(err => { resolve({success: false}); })
    });
  }

  /**
  * @name updateEmailById
  * @summary update user email by id
  * @param {string} id - user id
  * @param {string} email - user email
  * @return {object} promise - resolves to result or error
  */
  updateEmailById(id, email) {
    return new Promise((resolve, reject) => {

      mysqldb.query( 'UPDATE usuario SET email=\''+email+'\' WHERE id='+id)
      .then( row => {
          resolve({
              success: true
            });
            return;
        }, err => {
        resolve({success: false});
        })
      .catch(err => { resolve({success: false}); })
    });
  }

  /**
  * @name updatePassById
  * @summary update user password by id
  * @param {string} id - user id
  * @param {string} senha - user password
  * @return {object} promise - resolves to result or error
  */
  updatePassById(id, senha) {
    return new Promise((resolve, reject) => {

      mysqldb.query( 'UPDATE usuario SET senha=MD5(\''+senha+'\') WHERE id='+id )
      .then( row => {
          resolve({
              success: true
            });
            return;
        }, err => {
        resolve({success: false});
        })
      .catch(err => { resolve({success: false}); })
    });
  }

  /**
  * @name removeById
  * @summary remove user by id
  * @param {string} id - user id
  * @return {object} promise - resolves to result or error
  */
  removeById(id) {
    return new Promise((resolve, reject) => {

      mysqldb.query( 'DELETE FROM usuario WHERE id='+id )
      .then( row => {
          resolve({
              success: true
            });
            return;
        }, err => {
        resolve({success: false});
        } )
      .catch(err => { resolve({success: false}); })
    });
  }

  /**
  * @name create
  * @summary create new user
  * @param {string} email - user email
  * @param {string} senha - user pwd
  * @param {string} cep - user cep
  * @return {object} promise - resolves to result or error
  */
  create(email,senha,cep) {
    return new Promise((resolve, reject) => {

      mysqldb.query( 'INSERT INTO `usuario` (`email`, `senha`, `cep`) VALUES (\''+email+'\', MD5(\''+senha+'\'), '+cep+')' )
      .then( row => {
          resolve({
              success: true
            });
            return;
        }, err => {
        resolve({success: false});
        } )
      .catch(err => { resolve({success: false}); })
    });
  }
}

module.exports = Crud;
