const mysql = require( 'mysql' );
const mysqlConfig = require('../../dose-mysql-config.js');

/**
* @name Mysql
* @summary Mysql helper class
* @return {undefined}
*/

class Mysql {
  /**
  * @name constructor
  * @summary class constructor
  * @param {string} config -
  * @return {undefined}
  */
    constructor( config ) {
        this.connection = mysql.createConnection( {
    host     : mysqlConfig.database.host,
    port     : mysqlConfig.database.port,
    user     : mysqlConfig.database.user,
    password : mysqlConfig.database.password,
    database : mysqlConfig.database.database
  } );
    }
    /**
    * @name query
    * @summary Connect to Mysql instance
    * @param {string} sql - sql query
    * @param {string} args - optional
    * @return {object} promise
    */
    query( sql, args ) {
        return new Promise( ( resolve, reject ) => {
            this.connection.query( sql, args, ( err, rows ) => {
                if ( err )
                    return reject( err );
                resolve( rows );
            } );
        } );
    }
    /**
    * @name close
    * @summary Close Mysql client connection
    * @return {object} promise
    */
    close() {
        return new Promise( ( resolve, reject ) => {
            this.connection.end( err => {
                if ( err )
                    return reject( err );
                resolve();
            } );
        } );
    }
}


module.exports = new Mysql();
