# renan-dose-evalapp-service

Aplicação NODEJS(Hydra e Espress) com autenticação e CRUD de usuários {email, senha e CEP} num banco de dados MySQL

## Instalação

### NodeJS
```shell
$ cd renan-dose-evalapp-service
$ npm install
```
### Gerando Chaves para autenticação
Na raiz da aplicação, execute os seguintes comandos:
```shell
$ openssl genrsa -out server.pem 1024
$ openssl rsa -in server.pem -pubout > server.pub
$ chmod 600 server.pem
```

### MySQL
Instale o mysql-server, configure o root com o comando ```mysql_secure_installation``` e execute os seguintes comandos:
```shell
$ CREATE DATABASE dose;

$ USE dose;

$ CREATE TABLE IF NOT EXISTS usuario (
    `id` INT UNSIGNED AUTO_INCREMENT,
    `email` VARCHAR(48) NOT NULL,
    `senha` VARCHAR(255) NOT NULL,
    `cep` INT(8) NOT NULL,
    `last`_login VARCHAR(15) DEFAULT NULL,
    `token` VARCHAR(255) DEFAULT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY `email` (`email`)
)  ENGINE=INNODB DEFAULT CHARSET=utf8;

$ CREATE USER 'dose'@'localhost' IDENTIFIED BY 'password';

$ GRANT ALL PRIVILEGES ON dose.* TO 'dose'@'localhost' IDENTIFIED BY 'password';
```
Após isto, crie o arquivo ```dose-mysql-config.js``` na mesma pasta da aplicação:
```javascript
module.exports = {
  database:{
	  host : 'localhost',
	  port : 3306,
	  user : 'dose',
	  password : '', //utilize a senha do usuario definida no mysql
	  database : 'dose'
  }
}

```

## Execução

```shell
$ npm start
```
## Rotas

 1. Login  (Parâmetros: EMAIL e SENHA do usuário)
```shell
curl -d '{"email":USER,"senha":PASS}' -H "Content-Type: application/json" -X POST http://SERVICEIP:8888/v1/renan-dose-evalapp/login
 ```
 2. Logout (Parâmetros: TOKEN do usuário)
```shell
curl -d '{"token":TOKEN}' -H "Content-Type: application/json" -X POST http://SERVICEIP:8888/v1/renan-dose-evalapp/logout
 ```
 3. Lista Usuário por Id (Parâmetros: ID do usuário)
```shell
curl -X GET http://SERVICEIP:8888/v1/renan-dose-evalapp/crud/lista-usuario/{ID}
 ```
 4. Lista Usuários
```shell
curl -X GET http://SERVICEIP:8888/v1/renan-dose-evalapp/crud/lista-usuarios
 ```
 5. Atualiza Email (Parâmetros: ID e EMAIL do usuário)
```shell
curl -X PUT http://SERVICEIP:8888/v1/renan-dose-evalapp/crud/atualiza-email/{ID}/{EMAIL}
 ```
 6. Atualiza Senha (Parâmetros: ID e SENHA do usuário)
```shell
curl -d '{"id":ID,"senha":PASS}'  -H "Content-Type: application/json" -X POST http://SERVICEIP:8888/v1/renan-dose-evalapp/crud/atualiza-senha
 ```
 7. Atualiza CEP (Parâmetros: ID e CEP do usuário)
```shell
curl -X PUT http://SERVICEIP:8888/v1/renan-dose-evalapp/crud/atualiza-cep/{ID}/{CEP}
 ```
 8. Remove Usuário (Parâmetros: ID do usuário)
```shell
curl -X DELETE http://SERVICEIP:8888/v1/renan-dose-evalapp/crud/remove-usuario/{ID}
 ```
 9. Adiciona Usuário (Parâmetros: email, senha e CEP do usuário)
```shell
curl -d '{"email":EMAIL, "senha":SENHA, "cep":CEP}' -H "Content-Type: application/json" -X POST http://SERVICEIP:8888/v1/renan-dose-evalapp/crud/cria-usuario
 ```
