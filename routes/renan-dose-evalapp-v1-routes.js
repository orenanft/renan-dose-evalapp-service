/**
 * @name renan-dose-evalapp-v1-api
 * @description This module packages the Renan-dose-evalapp API.
 */
'use strict';

//require('request').debug = true

const hydraExpress = require('hydra-express');
const hydra = hydraExpress.getHydra();
const express = hydraExpress.getExpress();
const jwtAuth = require('fwsp-jwt-auth');
const ServerResponse = require('fwsp-server-response');

const Auth = require('../lib/auth');
const Crud = require('../lib/crud');

/**
* @name sendInvalidData
* @summary Helper function to return 401
* @param {object} res - express result object
* @param {string} reason - error explaination
* @return {undefined}
*/
function sendInvalidData(res, reason = 'Dados inválidos') {
  serverResponse.sendInvalidUserCredentials(res, {
    result: {
      isValid: false,
      reason
    }
  });
}

let serverResponse = new ServerResponse();
let api = express.Router();

let auth = new Auth();

/**
* @name login
* @summary Login route
* @param {object} req - express request object.
* @param {object} res - express response object
*/
api.post('/login', (req, res) => {
  if (!req.body.email || !req.body.senha) {
    sendInvalidData(res);
    return;
  }

  auth.login(req.body.email, req.body.senha)
    .then((result) => {
      if (!result.isValid) {
        sendInvalidData(res);
        return result;
      }
      serverResponse.sendOk(res, {
        result
      });
      return;
    })
    .catch((err) => {
      serverResponse.sendServerError(res, {result: {error: err}});
    });
});

/**
* @name logout
* @summary Logout route
* @param {object} req - express request object.
* @param {object} res - express response object
*/
api.post('/logout', (req, res) => {
  let token = req.body.token;
  if (!token) {
    sendInvalidData(res);
    return;
  }

  auth.logout(token)
    .then((result) => {
      if (!result.isValid) {
        sendInvalidData(res);
        return result;
      }
      serverResponse.sendOk(res, {
        result
      });
      return;
    })
    .catch((err) => {
      serverResponse.sendServerError(res, {result: {error: err}});
    });
});

let crud = new Crud();

/**
* @name listUserById
* @summary List user by Id route
* @param {object} req - express request object.
* @param {object} res - express response object
*/
api.get('/crud/lista-usuario/:id', (req, res) => {
  if (!req.params.id) {
    sendInvalidData(res);
    return;
  }

  crud.listById(req.params.id)
    .then((result) => {
      if (!result.isValid) {
        sendInvalidData(res);
        return result;
      }
      serverResponse.sendOk(res, {
        result
      });
      return;
    })
    .catch((err) => {
      serverResponse.sendServerError(res, {result: {error: err}});
    });
});

/**
* @name listUsers
* @summary List all users route
* @param {object} req - express request object.
* @param {object} res - express response object
*/
api.get('/crud/lista-usuarios', (req, res) => {

  crud.list()
    .then((result) => {
      if (!result.isValid) {
        sendInvalidData(res);
        return result;
      }
      serverResponse.sendOk(res, {
        result
      });
      return;
    })
    .catch((err) => {
      serverResponse.sendServerError(res, {result: {error: err}});
    });
});

/**
* @name updateUserCepById
* @summary Update user CEP by Id route
* @param {object} req - express request object.
* @param {object} res - express response object
*/
api.put('/crud/atualiza-cep/:id/:cep', (req, res) => {
  if (!req.params.id || !req.params.cep) {
    sendInvalidData(res);
    return;
  }

  crud.updateCepById(req.params.id, req.params.cep)
    .then((result) => {
      if (!result.success) {
        sendInvalidData(res);
        return result;
      }
      serverResponse.sendOk(res, {
        result
      });
      return;
    })
    .catch((err) => {
      serverResponse.sendServerError(res, {result: {error: err}});
    });
});

/**
* @name updateUserEmailById
* @summary Update user email by Id route
* @param {object} req - express request object.
* @param {object} res - express response object
*/
api.put('/crud/atualiza-email/:id/:email', (req, res) => {
  if (!req.params.id || !req.params.email) {
    sendInvalidData(res);
    return;
  }

  crud.updateEmailById(req.params.id, req.params.email)
    .then((result) => {
      if (!result.success) {
        sendInvalidData(res);
        return result;
      }
      serverResponse.sendOk(res, {
        result
      });
      return;
    })
    .catch((err) => {
      serverResponse.sendServerError(res, {result: {error: err}});
    });
});

/**
* @name updateUserPassById
* @summary Update user password by Id route
* @param {object} req - express request object.
* @param {object} res - express response object
*/
api.post('/crud/atualiza-senha', (req, res) => {
  if (!req.body.id || !req.body.senha) {
    sendInvalidData(res);
    return;
  }

  crud.updatePassById(req.body.id, req.body.senha)
    .then((result) => {
      if (!result.success) {
        sendInvalidData(res);
        return result;
      }
      serverResponse.sendOk(res, {
        result
      });
      return;
    })
    .catch((err) => {
      serverResponse.sendServerError(res, {result: {error: err}});
    });
});

/**
* @name removeUserById
* @summary Remove user by Id route
* @param {object} req - express request object.
* @param {object} res - express response object
*/
api.delete('/crud/remove-usuario/:id', (req, res) => {
  if (!req.params.id) {
    sendInvalidData(res);
    return;
  }

  crud.removeById(req.params.id)
    .then((result) => {
      if (!result.success) {
        sendInvalidData(res);
        return result;
      }
      serverResponse.sendOk(res, {
        result
      });
      return;
    })
    .catch((err) => {
      serverResponse.sendServerError(res, {result: {error: err}});
    });
});

/**
* @name CreateUser
* @summary Create user route
* @param {object} req - express request object.
* @param {object} res - express response object
*/
api.post('/crud/cria-usuario', (req, res) => {
  if (!req.body.email || !req.body.senha || !req.body.cep ) {
    sendInvalidData(res);
    return;
  }

  crud.create(req.body.email,req.body.senha,req.body.cep)
    .then((result) => {
      if (!result.success) {
        sendInvalidData(res);
        return result;
      }
      serverResponse.sendOk(res, {
        result
      });
      return;
    })
    .catch((err) => {
      serverResponse.sendServerError(res, {result: {error: err}});
    });
});
module.exports = api;
